/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Thibault Santonja
 * Created: 17 juin 2019
 */
/**
DROP TABLE subject CASCADE;
DROP TABLE website_user CASCADE;
DROP TABLE proposition CASCADE;
DROP TABLE consultation CASCADE;
DROP TABLE response CASCADE;
*/

CREATE TABLE IF NOT EXISTS subject (
  name          varchar(32),
  description   text
);

ALTER TABLE subject ADD CONSTRAINT cPkSubject PRIMARY KEY(name);


CREATE TABLE IF NOT EXISTS website_user (
  username      varchar(32),
  password      varchar(32),
  address       text,
  user_type     varchar(32),
  firstname     varchar(32),
  lastname      varchar(32),
  phone_number  varchar(10),
  creation_date text,
  state        varchar(32)
);

ALTER TABLE website_user ADD CONSTRAINT cPkUser PRIMARY KEY(username);
ALTER TABLE website_user ADD CONSTRAINT cUserType CHECK(user_type IN ('ADMIN', 'USER'));
ALTER TABLE website_user ADD CONSTRAINT cUserState CHECK(state IN ('ACTIVE', 'INACTIVE'));


CREATE TABLE IF NOT EXISTS proposition (
  num           varchar(32),
  fk_subject    varchar(32),
  fk_author     varchar(32),
  state         varchar(32),
  comment       text
);

ALTER TABLE proposition ADD CONSTRAINT cPkProposition PRIMARY KEY(num);
ALTER TABLE proposition ADD CONSTRAINT cPropositionState CHECK(state IN ('ACTIVE', 'INACTIVE'));
ALTER TABLE proposition ADD CONSTRAINT cFkAuthor FOREIGN KEY (fk_author) REFERENCES website_user(username);
ALTER TABLE proposition ADD CONSTRAINT cFkSubject FOREIGN KEY (fk_subject) REFERENCES subject(name);


CREATE TABLE IF NOT EXISTS consultation (
  num           varchar(32),
  fk_user       varchar(32),
  fk_proposition varchar(32)
);

ALTER TABLE consultation ADD CONSTRAINT cPkConsultation PRIMARY KEY(num);
ALTER TABLE consultation ADD CONSTRAINT cFkUserConsultation FOREIGN KEY (fk_user) REFERENCES website_user(username);
ALTER TABLE consultation ADD CONSTRAINT cFkProposition FOREIGN KEY (fk_proposition) REFERENCES proposition(num);


CREATE TABLE IF NOT EXISTS response (
  num           varchar(32),
  fk_consultation varchar(32),
  fk_user       varchar(32),
  choice        varchar(32),
  comment       text
);

ALTER TABLE response ADD CONSTRAINT cPkResponse PRIMARY KEY(num);
ALTER TABLE response ADD CONSTRAINT cFkConsultation FOREIGN KEY (fk_consultation) REFERENCES consultation(num);
ALTER TABLE response ADD CONSTRAINT cFkUserResponse FOREIGN KEY (fk_user) REFERENCES website_user(username);